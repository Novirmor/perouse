#! /bin/sh
iptables -t nat -A PREROUTING -i eth0 -p tcp --dport 8080 -j DNAT --to ${IP}:${PORT}